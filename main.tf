provider "aws" {
    region = "us-east-1"
}

variable "cidr_blocks"{
  description = "cidr_block_for_subnet"
  type = list(object(
  {
    cidr_block = string
    name = string
  }))
}


resource "aws_vpc" "db_vpc" {
  cidr_block = var.cidr_blocks[0].cidr_block
  tags = {
    Name = var.cidr_blocks[0].name
    vpc-env = "dbdev"
  }
}

resource "aws_subnet" "subnet_1" {
  vpc_id     = aws_vpc.db_vpc.id
  cidr_block = var.cidr_blocks[1].cidr_block
  tags = {
    Name = var.cidr_blocks[1].name
    subnet-env = "dbsubnet1dev"
  }
}

output "print-vpc-id" {
  value = aws_vpc.db_vpc.id
  
}

output "print-subnet-id" {
  value = aws_subnet.subnet_1.id
  
}




